#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    // TODO: FILL THIS IN
    char pass[50];
    char hash[33];
};

int entry_cmp (const void *a,const void *b)
{
    return strcmp((*(struct entry *)a).hash,(*(struct entry *)b).hash);
    
}

int entry_to_str_cmp(const void *b,const void *a)
{
    
    //printf("comparing stored hash %s\n      with given hash %s\n\n",(*(struct entry *)a).hash,((char *)b));
    return -(strcmp((*(struct entry *)a).hash,((char *)b)));
    
}

int file_length(char *filename)
{
    struct stat info;
    int ret = stat(filename, &info);
    if (ret == -1) return ret;
    else return info.st_size;
}
// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    int len = file_length(filename);
    char* passes=malloc(len);
    FILE *f = fopen(filename, "r");
    if (!f){
        printf("Can't open password list for reading\n");
        exit(1);
    }
    fread(passes, sizeof(char), len, f);
    fclose(f);
    
    int num_passes = 0;
     
    for (int i = 0; i < len; i++)
    {
        if (passes[i] == '\n')
        {
            passes[i] = '\0';
            num_passes++;
        }
    }
    // Change newlines into nulls and count them 
    // as we go.

    struct entry * entries = malloc(num_passes*sizeof(struct entry));
    sscanf(&passes[0],"%s", entries[0].pass);  //set the first entry to the first pass
    strcpy(entries[0].hash,md5(entries[0].pass,strlen(entries[0].pass)));//entries[0].hash=md5(entries[0].pass,strlen(entries[0].pass));
    int j = 1;
    for (int i = 0; i < len-1; i++)
    {
        if (passes[i] == '\0')
        {
            sscanf(&passes[i+1], "%s", entries[j].pass);
            strcpy(entries[j].hash,md5(entries[j].pass,strlen(entries[j].pass)));   //entries[j].hash=md5(entries[j].pass,strlen(entries[j].pass));
            
            j++;
        }
    }
    
    //loop through 
    //entries[j].password=&file[i+1];
    //entries[j].hash = md5(word, strlen(word));
    *size = num_passes;
    return entries;
    //returns an array of all the entries, and size of that array
}


void find_hashes(struct entry *entries,char *hashfilename, int size)
{
    FILE *hashfile = fopen(hashfilename, "r");
    if (!hashfile){
        printf("Can't open password list for reading\n");
        exit(1);
    }
    // TODO
    int len = file_length(hashfilename);
    char* hashes=malloc(len);
    fread(hashes, sizeof(char), len, hashfile);
    
    fclose(hashfile);
    int startofhash=0;
    for (int i = 0; i < len; i++)
    {
        if (hashes[i] == '\n')
    {
            hashes[i] = '\0';
            
            struct entry *found = bsearch(&hashes[startofhash],entries,size,sizeof(struct entry),entry_to_str_cmp);
            if (found != NULL)
            {
                printf("Cracked %s:  %s\n", (*found).hash, (*found).pass);
            }
            else
            {
                printf("No matches found for hash %s\n",&hashes[startofhash]);
            }
            startofhash=i+1;
        }
    }
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)

}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    int num_passes = 0;
    // TODO: Read the dictionary file into an array of entry structures
    struct entry *dict = read_dictionary(argv[2], &num_passes);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, num_passes, sizeof(struct entry), entry_cmp);
    /*for (int i=0;i<num_passes;i++)   //for debugging purposes
    {
        printf("Processed Hash:  %s  ,  Pass:   %s\n",dict[i].hash, dict[i].pass);
        
        
    }*/
    // TODO
    // Open the hash file for reading.
    //printf("%s",argv[1]);
    find_hashes(dict, argv[1],num_passes);
    //may need another loop to free things at the end
}
